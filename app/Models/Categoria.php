<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //Consult for slug
    public function getRouteKeyName()
    {
        return 'slug';
    }

    //Relation Category 1:n para establecimientos
    public function establecimientos(){
        return $this->hasMany(Establecimiento::class);
    }



}
