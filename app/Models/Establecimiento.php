<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    protected $fillable = [
         'nombre',
        'categoria_id',
        'imagen_principal',
        'direccion',
        'barrio',
        'lat',
        'lng',
        'telefono',
        'descripcion',
        'apertura',
        'cierre',
        'uuid'
    ];

    //Pertenece a categoria_id a categoria id
    public function  categoria(){
        return $this->belongsTo(Categoria::class);
    }
}
