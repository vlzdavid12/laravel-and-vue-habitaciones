<?php

namespace App\Http\Controllers;

use App\Models\Establecimiento;
use App\Models\Imagen;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImagenController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Read Image
        $ruta_imagen = $request->file('file')->store('establecimientos', 'public');

        //Resize Image
        $imagen = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800, 450);

        $imagen->save();

        //Insert model
        $imageDB = new Imagen;
        $imageDB->id_establecimiento = $request['uuid'];
        $imageDB->url_imagen = $ruta_imagen;

        $imageDB->save();

        $respuesta=[
            "archive" => $ruta_imagen,
            "id"=>$request['uuid']
        ];

        return response()->json($respuesta);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Imagen  $imagen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Imagen $imagen)
    {
        //Access Request Params
        $uuid = $request->get('uuid');
        $establecimiento = Establecimiento::where('uuid', $uuid)->first();

        $this->authorize('delete', $establecimiento);

        $resp = $request->get('imagen');

        if(File::exists('storage/'.$resp)){
            //Delete Image Server
            File::delete('storage/'.$resp);
            //Delete Image DB
            //Imagen::where('url_imagen', '=', $resp)->delete();
            //$imagenDB= Imagen::where('url_imagen', "=", $resp)->firstOrFail();
            //Imagen::destroy($imagenDB->id);
            Imagen::where('url_imagen', $imagen)->delete();
            //Delete Image DB
            $result= [
                'msg' => "Imagen Eliminado",
                "imagen" => $resp,
                "uuid" => $uuid
            ];
        }

        return  response()->json($result);
    }
}
