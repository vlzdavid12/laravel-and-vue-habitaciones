<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Establecimiento;
use App\Models\Imagen;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class EstablecimientoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all();

        return view('establecimientos.index', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
       //Validate Form
        $data = $request->validate([
            "nombre" => "required|min:10",
            "categoria" => "required|exists:App\Models\Categoria,id",
            "imagen_principal" => "required|image|max:1000",
            "direccion" => "required",
            "ubicacion" => "required",
            "telefono" => "required|integer",
            "descripcion" => "required|min:50",
            "apertura" => "date_format:H:i",
            "cierre" => "date_format:H:i|after:apertura",
            "uuid"=>"required",
            "lat"=>"required",
            "lng"=>"required"

        ]);

        //Save Imagen
        $ruta_imagen = $request['imagen_principal']->store('principales', 'public');

        //Resize Imagen
        $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800, 600);
        $img->save();

        //Insert Save DB Establecimientos
        auth()->user()->establecimiento()->create([
            "nombre" => $data['nombre'],
            "categoria_id" => $data['categoria'] ,
            "imagen_principal" => $ruta_imagen,
            "direccion" => $data['direccion'],
            "barrio" => $data['ubicacion'],
            "telefono" => $data['telefono'],
            "descripcion" => $data['descripcion'],
            "apertura" => $data['apertura'],
            "cierre" => $data['cierre'] ,
            "uuid"=> $data['uuid'],
            "lat"=> $data['lat'],
            "lng"=> $data['lng']
        ]);

        return back()->with('state', 'Tu información a sido guardado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Establecimiento $establecimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Establecimiento $establecimiento)
    {
        $categorias = Categoria::all();

        //Get id Establecimiento
        $establecimiento =  Auth()->user()->establecimiento;
        $establecimiento->apertura = date('H:i', strtotime($establecimiento->apertura));
        $establecimiento->cierre = date('H:i', strtotime($establecimiento->cierre));


        //Get image of establecimiento
        $images = Imagen::where('id_establecimiento', $establecimiento->uuid)->get();


        return view('establecimientos.edit', compact('categorias', 'establecimiento', 'images'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Establecimiento $establecimiento)
    {
        //Execute of policy
        $this->authorize('update', $establecimiento);

        //Validate Form
        $data = $request->validate([
            "nombre" => "required|min:10",
            "categoria" => "required|exists:App\Models\Categoria,id",
            "imagen_principal" => "image|max:1000",
            "direccion" => "required",
            "ubicacion" => "required",
            "telefono" => "required|integer",
            "descripcion" => "required|min:50",
            "apertura" => "date_format:H:i",
            "cierre" => "date_format:H:i|after:apertura",
            "uuid"=>"required",
            "lat"=>"required",
            "lng"=>"required"

        ]);

        $establecimiento->nombre = $data['nombre'];
        $establecimiento->categoria_id = $data['categoria'];
        $establecimiento->direccion = $data['direccion'];
        $establecimiento->barrio = $data['ubicacion'];
        $establecimiento->lat = $data['lat'];
        $establecimiento->lng = $data['lng'];
        $establecimiento->telefono = $data['telefono'];
        $establecimiento->descripcion = $data['descripcion'];
        $establecimiento->apertura = $data['apertura'];
        $establecimiento->cierre = $data['cierre'];
        $establecimiento->uuid = $data['uuid'];

        if(request('imagen_principal')){
            //Save Imagen FILE
            $ruta_image = $request['imagen_principal']->store('principales', 'public');

            //Resize Image
            $img = Image::make(public_patch("storage/{$ruta_image}"))->fit(800, 600);
            $img->save();

            $establecimiento->imagen_principal = $ruta_image;
        }


        $establecimiento->save();
        //Message save success fully
        return back()->with('state', 'Se ha actualizado correctamente los datos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Establecimiento  $establecimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Establecimiento $establecimiento)
    {
        //
    }
}
