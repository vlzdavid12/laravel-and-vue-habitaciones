<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Establecimiento;
use App\Models\Imagen;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function index(){
            $establemientos = Establecimiento::with('categoria')->get();
            return response()->json($establemientos);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCategorys(){
        $category = Categoria::all();
        return response()->json($category);
    }

    /**
     * @param Categoria $categoria
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCategory(Categoria $categoria){
        $establecimiento = Establecimiento::where('categoria_id', $categoria->id)->with('categoria')->take(3)->get();

        return response()->json($establecimiento);
    }

    /**
     * @param Categoria $categoria
     * @return \Illuminate\Http\JsonResponse
     */

    public function showEstablecimientoCategory(Categoria $categoria){

        $establecimiento = Establecimiento::where('categoria_id', $categoria->id)->with('categoria')->get();

        return response()->json($establecimiento);
    }


    /**
     * @param Establecimiento $establecimiento
     * @return \Illuminate\Http\JsonResponse
     */

    public function show(Establecimiento  $establecimiento){

        $imagenes = Imagen::where('id_establecimiento', $establecimiento->uuid)->get();
        $establecimiento->images = $imagenes;
        return response()->json($establecimiento);

    }


}
