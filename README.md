# System Habitaciones Vue and Laravel

![alt text](https://gitlab.com/vlzdavid12/laravel-and-vue-habitaciones/-/raw/master/ScreenShot.png)


## Install


Download the folder or zip from the repository.

```
composer install
```

Go to the root repository folder

```
cd habitaciones
npm install
npm run watch
or
npm run dev
```

## Start Server Laravel 🚀

```
php artisan serve

```

## Error 500 😣
It is possible that the error 500.

.env.example for .env

```
php artisan key:generate --show
php artisan key:generate
```

## Error Image 😖
You may get an error in the image enter this command 🤫, Don't tell anyone!
```
php artisan storage:link
```


