<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**List Api**/
Route::get('/categorias/', [App\Http\Controllers\ApiController::class, 'showCategorys'])->name('categorys');
Route::get('/categorias/{categoria}', [App\Http\Controllers\ApiController::class, 'showCategory'])->name('category');
Route::get('/categoria/{categoria}', [App\Http\Controllers\ApiController::class, 'showEstablecimientoCategory'])->name('category.establecimientocategory');

Route::get('/establecimientos/', [App\Http\Controllers\ApiController::class, 'index'])->name('establecimiento.index');
Route::get('/establecimiento/{establecimiento}', [App\Http\Controllers\ApiController::class, 'show'])->name('establecimiento.show');
