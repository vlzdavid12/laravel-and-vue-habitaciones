<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', App\Http\Controllers\InicioController::class)->name('inicio');

Auth::routes(['verify'=> true]);

Route::group(['middleware' => ['auth', 'verified']], function(){
    Route::get('/establecimiento/create', [App\Http\Controllers\EstablecimientoController::class, 'create'])->name('establecimiento.create')->middleware('check');
    Route::get('/establecimiento/edit', [App\Http\Controllers\EstablecimientoController::class, 'edit'])->name('establecimiento.edit');
    Route::post('/establecimiento/', [App\Http\Controllers\EstablecimientoController::class, 'store'])->name('establecimiento.store');
    Route::put('/establecimiento/{establecimiento}', [App\Http\Controllers\EstablecimientoController::class, 'update'])->name('establecimiento.update');

    Route::post('/imagen/store', [App\Http\Controllers\ImagenController::class, 'store'])->name('imagen.store');
    Route::post('/imagen/destroy', [App\Http\Controllers\ImagenController::class, 'destroy'])->name('establecimiento.destroy');
});

