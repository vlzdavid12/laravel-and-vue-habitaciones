<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::TABLE('users')->insert([
            'name' => 'David Valenzuela',
            'email' => 'vlzdavid12@outlook.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('valenzuela21'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('users')->insert([
            'name' => 'Juan Pablo',
            'email' => 'juan@outlook.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('valenzuela21'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
