<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::TABLE('categorias')->insert([
                    'nombre' => 'Restaurant',
                    'slug' => Str::slug('Restaurant'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Bar',
            'slug' => Str::slug('Bar'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Consultorios',
            'slug' => Str::slug('Consultorios'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Café',
            'slug' => Str::slug('Café'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Gimnasio',
            'slug' => Str::slug('Gimnasio'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Hospitales',
            'slug' => Str::slug('Hospitales'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('categorias')->insert([
            'nombre' => 'Hoteles',
            'slug' => Str::slug('Hoteles'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
