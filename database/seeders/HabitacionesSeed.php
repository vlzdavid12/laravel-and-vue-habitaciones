<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HabitacionesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edicios De Los Alcasares',
            'categoria_id' => 1,
            'imagen_principal' => 'principales/7GPO5rmWifhbC9XcwYDyiGyn9gZT3frSCrVB6VkH.png',
            'direccion' => 'Carrera 101 146A 38',
            'barrio' => 'El pino',
            'lat' => 4.7479215003335655,
            'lng' => -74.09047419479093,
            'telefono' => 3213465789,
            'descripcion' => "What is Lorem Ipsum?
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                              printer took a galley of type and scrambled it to make a type specimen book.",
            'apertura' => '08:00:00',
            'cierre' => '09:00:00',
            'uuid' => '9ef549c4-062a-4e8a-aa00-5d2b234dcaf1',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Chapinero Alto',
            'categoria_id' => 2,
            'imagen_principal' => 'principales/7GPO5rmWifhbC9XcwYDyiGyn9gZT3frSCrVB6VkH.png',
            'direccion' => 'Calle 51 23 70',
            'barrio' => 'Alfonso Lopez',
            'lat' => 4.6397753443796645,
            'lng' => -74.07411036115403,
            'telefono' => 34678904556,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '05:00:00',
            'cierre' => '12:00:00',
            'uuid' => 'bb9e2863-6a4c-4839-8fd5-fe69b6ffc0df',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edicios De Los Alcasares',
            'categoria_id' => 3,
            'imagen_principal' => 'principales/CxDe5Mpf8EtJ2ENyHuE5H9Z5j2WVgMe2l927KXUi.png',
            'direccion' => 'Avenida Carrera 24 65 58',
            'barrio' => 'Siete de Agosto',
            'lat' => 4.65703000000002,
            'lng' => -74.07079999999996,
            'telefono' => 4758699474,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '08:00:00',
            'cierre' => '09:00:00',
            'uuid' => 'bf1522fc-66e9-4269-9712-9e97d48eddbc',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edificios Zona Franca',
            'categoria_id' => 4,
            'imagen_principal' => 'principales/L45Jd2VIGLtiTPj8MfFG0uqJhVGp2Cexanrqvjsg.png',
            'direccion' => 'Cra 106, Casillero 3 #15A 25,',
            'barrio' => 'Zona Franca',
            'lat' => 4.6711447,
            'lng' => -74.1787386,
            'telefono' => 3213456789,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '06:30:00',
            'cierre' => '09:30:00',
            'uuid' => '732dc531-bc78-41a8-b95c-2a1291480edc',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edificios 7 de Agosto',
            'categoria_id' => 5,
            'imagen_principal' => 'principales/3pwx5VxRxj34rGCffERQWgs29ySqNTOr2HgTN1pQ.png',
            'direccion' => 'Cl. 66 #27 - 05',
            'barrio' => '7 de Agosto',
            'lat' => 4.6580091,
            'lng' => -74.0749537,
            'telefono' => 32456890,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '07:30:00',
            'cierre' => '08:30:00',
            'uuid' => '6d133a02-bec8-4654-947c-fb8d5606760c',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Clinica Reina Sofia',
            'categoria_id' => 6,
            'imagen_principal' => 'principales/mwdzEij7FbDKCXLALxRWT3hhSmWSlBldLxiIIT4y.png',
            'direccion' => 'Cl 127 #20-78, Bogotá',
            'barrio' => 'Unicentro',
            'lat' => 4.6922348,
            'lng' => -74.0597296,
            'telefono' => 32456890,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '07:30:00',
            'cierre' => '08:30:00',
            'uuid' => '88e5343e-08e4-4c69-8be1-cb6334337ebf',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edificos Bernardino CVI',
            'categoria_id' => 1,
            'imagen_principal' => 'principales/rPZkpg2s54mL5Lsp0FXrjl0oIEQ1hxqmEqdmpbo2.png',
            'direccion' => 'Cl 127 #20-78, Bogotá',
            'barrio' => 'Bosa Recreo',
            'lat' => 4.640934,
            'lng' => -74.2016639,
            'telefono' => 243566778,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '10:30:00',
            'cierre' => '12:30:00',
            'uuid' => '18625325-b816-41f1-8af2-7d21ffa0596e',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Edificos Palermo 45',
            'categoria_id' => 2,
            'imagen_principal' => 'principales/rPZkpg2s54mL5Lsp0FXrjl0oIEQ1hxqmEqdmpbo2.png',
            'direccion' => '44, Cl. 45 #9, Bogotá',
            'barrio' => 'Centro Ciudad',
            'lat' => 4.6323123,
            'lng' => -74.0705839,
            'telefono' => 34568970,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '9:30:00',
            'cierre' => '12:30:00',
            'uuid' => '2207e2db-2f78-4bca-bb40-fb50ae272771',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Abadia La Candelaria',
            'categoria_id' => 3,
            'imagen_principal' => 'principales/rPZkpg2s54mL5Lsp0FXrjl0oIEQ1hxqmEqdmpbo2.png',
            'direccion' => 'Cra. 7 #6a52, Bogotá',
            'barrio' => 'Centro Candelaria',
            'lat' => 4.5894436,
            'lng' => -74.0801674,
            'telefono' => 4568790,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '9:30:00',
            'cierre' => '12:30:00',
            'uuid' => '006c2780-096c-45cc-b84c-d0fc1fdf4a50',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Hotel Junior Aeropuerto Bogota',
            'categoria_id' => 7,
            'imagen_principal' => 'principales/rPZkpg2s54mL5Lsp0FXrjl0oIEQ1hxqmEqdmpbo2.png',
            'direccion' => 'Dg. 25g #95 - 66, Bogotá, Cundinamarca',
            'barrio' => 'Centro Candelaria',
            'lat' => 4.6807649,
            'lng' => -74.1243287,
            'telefono' => 321345678,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '6:30:00',
            'cierre' => '12:30:00',
            'uuid' => '41ccf9ea-3473-4fd6-a5f8-38be59ba0aaa',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('establecimientos')->insert([
            'nombre' => 'Hotel Centro Plaza',
            'categoria_id' => 7,
            'imagen_principal' => 'principales/7GPO5rmWifhbC9XcwYDyiGyn9gZT3frSCrVB6VkH.png',
            'direccion' => 'Cra. 4 #12 74, Bogotá, DC',
            'barrio' => 'Centro',
            'lat' => 4.5863961,
            'lng' => -74.1102296,
            'telefono' => 321345678,
            'descripcion' => "Where can I get some?
                              There are many variations of passages of Lorem Ipsum available,
                              but the majority have suffered alteration in some form,
                              by injected humour, or randomised words which don't look
                              even slightly believable. If you are going to use a passage of
                              Lorem Ipsum, you need to be sure there isn't anything embarrassing
                              hidden in the middle of text. All the Lorem Ipsum generators on
                              the Internet tend to repeat predefined chunks as necessary, making
                              this the first true generator on the Internet. ",
            'apertura' => '6:30:00',
            'cierre' => '12:30:00',
            'uuid' => 'ef16a647-3b66-4e94-a244-ceb596d580be',
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
