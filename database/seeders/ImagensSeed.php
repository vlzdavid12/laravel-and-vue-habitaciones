<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ImagensSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => 'bb9e2863-6a4c-4839-8fd5-fe69b6ffc0df',
            'url_imagen' => 'establecimientos/Efn8cdoGwqPcGMYsc0HFlbuXyI9NMPRLLEk0ycwc.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => 'bb9e2863-6a4c-4839-8fd5-fe69b6ffc0df',
            'url_imagen' => 'establecimientos/KJIYGubqljYYDzLas0u5UHebR9AZQVzWOq3bl2VL.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '9ef549c4-062a-4e8a-aa00-5d2b234dcaf1',
            'url_imagen' => 'establecimientos/ROHlunjsK57C6qjurPnBtvidkFXO33zeR3k4qmIH.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => 'bf1522fc-66e9-4269-9712-9e97d48eddbc',
            'url_imagen' => 'establecimientos/L45Jd2VIGLtiTPj8MfFG0uqJhVGp2Cexanrqvjsg.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '6d133a02-bec8-4654-947c-fb8d5606760c',
            'url_imagen' => 'establecimientos/dugNa8le76g7JJNg0vQqEN9gz7jIQl84FF59GD71.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '88e5343e-08e4-4c69-8be1-cb6334337ebf',
            'url_imagen' => 'establecimientos/dugNa8le76g7JJNg0vQqEN9gz7jIQl84FF59GD71.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '732dc531-bc78-41a8-b95c-2a1291480edc',
            'url_imagen' => 'establecimientos/XW9AAGYiMkjmjKYp9Ty2jGxNGfyix82VbdWdsSLD.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '18625325-b816-41f1-8af2-7d21ffa0596e',
            'url_imagen' => 'establecimientos/epoiUnSyr9ktI1H4y4pCBOEKNOH19478UiJAV4cn.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '006c2780-096c-45cc-b84c-d0fc1fdf4a50',
            'url_imagen' => 'establecimientos/epoiUnSyr9ktI1H4y4pCBOEKNOH19478UiJAV4cn.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '006c2780-096c-45cc-b84c-d0fc1fdf4a50',
            'url_imagen' => 'establecimientos/L45Jd2VIGLtiTPj8MfFG0uqJhVGp2Cexanrqvjsg.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '41ccf9ea-3473-4fd6-a5f8-38be59ba0aaa',
            'url_imagen' => 'establecimientos/epoiUnSyr9ktI1H4y4pCBOEKNOH19478UiJAV4cn.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => '41ccf9ea-3473-4fd6-a5f8-38be59ba0aaa',
            'url_imagen' => 'establecimientos/XW9AAGYiMkjmjKYp9Ty2jGxNGfyix82VbdWdsSLD.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::TABLE('imagens')->insert([
            'id_establecimiento' => 'ef16a647-3b66-4e94-a244-ceb596d580be',
            'url_imagen' => 'establecimientos/KJIYGubqljYYDzLas0u5UHebR9AZQVzWOq3bl2VL.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
