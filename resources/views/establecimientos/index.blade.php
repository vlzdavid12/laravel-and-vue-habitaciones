@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""/>
    <link
        rel="stylesheet"
        href="https://unpkg.com/esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css"
    />
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css"
          integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
          crossorigin="anonymous" />
@endsection

@section('content')
    <div class="container text-center mt-5">
        <h2 class="text-center text-secondary">{{__('Crear Establecimiento')}}</h2>
        <div class="mt-5 row justify-content-center">
            <form enctype="multipart/form-data" action="{{route('establecimiento.store')}}" method="POST" class="col-md-9 col-xs-12 card card-body">
                @csrf
                <fieldset class="border p-4">
                    <legend class="text-primary text-left pl-2">{{__('Nombre Y Categoria, Imagen Principal')}}</legend>
                    <div class="form-group text-left">
                        <label for="nombre" class="py-1">{{__('Nombre Establecimiento')}}</label>
                        <input id="nombre"
                               type="text"
                               placeholder="Nombre del establecimiento"
                               name="nombre"
                               value="{{old('nombre')}}"
                               class="form-control @error('nombre') is-invalid @enderror"/>

                        @error('nombre')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group text-left mt-2">
                        <label for="categoria" class="py-1">{{__('Categoria')}}</label>
                        <select
                            id="categoria"
                            name="categoria"
                            class="form-control @error('categoria') is_invalid @enderror">
                            <option value="" >Selecciones Categoria</option>
                            @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}" @if(old('categoria') == $categoria->id ) selected @endif >{{$categoria->nombre}}</option>
                            @endforeach
                        </select>
                        @error('categoria')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group text-left">
                        <label for="imagen_principal" class="py-1">{{__('Imagen Principal')}}</label>
                        <input id="imagen_principal"
                               type="file"
                               name="imagen_principal"
                               class="form-control @error('imagen_principal') is-invalid @enderror"/>

                        @error('imagen_principal')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </fieldset>
                <fieldset class="border p-4 mt-3">
                    <legend class="text-primary text-left pl-2">{{__('Ubicación')}}</legend>
                    <div class="form-group text-left">
                        <label for="searchubicacion" class="py-1">{{__('Coloca la dirección del establecimiento')}}</label>
                        <input id="searchubicacion"
                               type="text"
                               placeholder="Example: Carrera 101 147 84 suba"
                               name="searchubicacion"
                               class="form-control"/>
                        <p class="text-secondary pl-1 py-2 text-center">
                            {{__('El asistente colocará un direccion estimada,
                            mueve el PIN hacia el lugar correcto.')}}</p>
                    </div>
                    <div class="form-group">
                        <div id="mapa" style="width: 100%; height: 580px;">
                        </div>
                    </div>
                    <p class="informacion">{{__('Confirma que los siguientes campos son correctos.')}}</p>
                    <div class="form-group text-left">
                        <label for="direccion">{{__('Dirección')}}</label>
                        <input type="text"
                               id="direccion"
                               name="direccion"
                               placeholder="Dirección adomicimilio"
                               value="{{old('direccion')}}"
                               class="form-control @error('direccion') is-invalid @enderror"
                               readonly
                        />
                        @error('direccion')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group text-left">
                        <label for="ubicacion">{{__('Ubicación')}}</label>
                        <input type="text"
                               id="ubicacion"
                               name="ubicacion"
                               placeholder="Sector o ubicación"
                               value="{{old('ubicacion')}}"
                               class="form-control @error('ubicacion') is-invalid @enderror"
                               readonly
                        />
                        @error('ubicacion')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <input type="hidden"
                           id="lat"
                           name="lat"
                           value="{{old('lat')}}"
                    />
                    <input type="hidden"
                           id="lng"
                           name="lng"
                           value="{{old('lng')}}"
                    />
                </fieldset>
                <fieldset class="border p-4 mt-5 text-left">
                    <legend  class="text-primary pl-2">{{__('Información Establecimiento:')}} </legend>
                    <div class="form-group">
                        <label for="telefono">{{__('Teléfono')}}</label>
                        <input
                            type="tel"
                            class="form-control @error('telefono')  is-invalid  @enderror"
                            id="telefono"
                            placeholder="Teléfono Establecimiento"
                            name="telefono"
                            value="{{ old('telefono') }}"
                        >

                        @error('telefono')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="descripcion">{{__('Descripción')}}</label>
                        <textarea
                            class="form-control  @error('descripcion')  is-invalid  @enderror"
                            name="descripcion"
                        >{{ old('descripcion') }}</textarea>

                        @error('descripcion')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="apertura">{{__('Hora Apertura:')}}</label>
                        <input
                            type="time"
                            class="form-control @error('apertura')  is-invalid  @enderror"
                            id="apertura"
                            name="apertura"
                            value="{{ old('apertura') }}"
                        >
                        @error('apertura')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="cierre">{{__('Hora Cierre:')}}</label>
                        <input
                            type="time"
                            class="form-control @error('cierre')  is-invalid  @enderror"
                            id="cierre"
                            name="cierre"
                            value="{{ old('cierre') }}"
                        >
                        @error('cierre')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </fieldset>

                <fieldset class="border p-4 mt-5 text-left">
                    <legend  class="text-primary pl-2">{{__('Imagenes Establecimiento: ')}}</legend>
                    <div class="form-group">
                        <label  for="imagenes">{{__('Imagenes')}}</label>
                        <div id="dropzone" class="dropzone from-control"></div>
                    </div>

                </fieldset>

                <input type="hidden" id="uuid" name="uuid" value="{{Str::uuid()->toString()}}" />
                <input type="submit" class="btn btn-primary mt-3" value="Registrar Establecimiento" />
            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin="" defer ></script>
    <script src="https://unpkg.com/esri-leaflet" defer ></script>
    <script src="https://unpkg.com/esri-leaflet-geocoder" defer ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"
            integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w=="
            crossorigin="anonymous" defer></script>
@endsection

