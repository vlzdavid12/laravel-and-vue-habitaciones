
document.addEventListener('DOMContentLoaded', ()=>{
    if(document.querySelector('div#dropzone')){
        Dropzone.autoDiscover = false;

        const token = document.querySelector('meta[name=csrf-token]').content
        const dropzone = new Dropzone('div#dropzone', {
            url: '/imagen/store',
            dictDefaultMessage: 'Sube hasta 10 imágenes',
            maxFiles: 10,
            required: true,
            acceptedFiles: ".png,.jpg,.jpeg,.bmp",
            addRemoveLinks: true,
            dictRemoveFile: "Eliminar Imagen",
            headers:{
                'X-CSRF-TOKEN': token
            },
            init: function(){
                const gallery = document.querySelectorAll('.gallery');
                if(gallery.length > 0){
                    gallery.forEach(image => {
                        const imagePublic = {};
                        imagePublic.size = 1;
                        imagePublic.name = image.value;
                        imagePublic.nameServer = image.value;
                        this.options.addedfile.call(this, imagePublic);
                        this.options.thumbnail.call(this, imagePublic, `/storage/${imagePublic.name}`);


                        imagePublic.previewElement.classList.add('dz-success')
                        imagePublic.previewElement.classList.add('dz-complete')
                    })
                }
            },
            success: function(file, resp){
                file.nameServer = resp.archive;
            },
            sending: function (file, xhr, formData){
                formData.append('uuid', document.querySelector('#uuid').value);
                console.log('Enviando...');
            },
            removedfile: function(file, resp){
                const params = {
                    imagen: file.nameServer,
                    uuid: document.querySelector('#uuid').value
                }

                axios.post(`/imagen/destroy`, params)
                    .then((resp) =>{
                        file.previewElement.parentNode.removeChild(file.previewElement);
                        console.log(resp);
                    })
                    .catch((error)=>console.log(error))
            }

        })
    }
})
