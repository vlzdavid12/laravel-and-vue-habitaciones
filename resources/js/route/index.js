import Vue from 'vue'
import VueRouter from  'vue-router'
import inicioEstablecimientos from "../components/inicioEstablecimientos";
import mostrarEstablecimiento from "../components/mostrarEstablecimiento";
import VuePageTransition from 'vue-page-transition'
const routes = [
    {
        path: '/',
        component: inicioEstablecimientos
    },{
        path: '/establecimientos/:id',
        name: 'establecimiento',
        component: mostrarEstablecimiento
    }
]

const router = new VueRouter({
        mode:'history',
        routes
})

Vue.use(VuePageTransition)
Vue.use(VueRouter)

export default router;
