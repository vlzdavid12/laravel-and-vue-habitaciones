import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        cafes: [],
        hotels: [],
        restaurants: [],
        establecimiento: {},
        establecimientos: [],
        categorias:[],
        categoria:''
    },
    mutations: {
        AGREGAR_CAFES(state, cafes) {
            state.cafes = cafes
        },
        AGREGAR_RESTAURANS(state, restaurants) {
            state.restaurants = restaurants
        },
        AGREGAR_HOTELS(state, hotels) {
            state.hotels = hotels
        },
        AGREGAR_EXTABLECIMIENTO(state, establecimiento) {
            state.establecimiento = establecimiento
        },
        AGREGAR_ESTABLECIMIENTOS(state, establecimientos){
            state.establecimientos = establecimientos
        },
        AGREGAR_CATEGORIA(state, categorias){
            state.categorias = categorias
        },
        SELECCIONAR_CATEGORIA(state, categoria){
            state.categoria = categoria
        }
    },
    getters: {
        getStablecimiento: state => {
            return state.establecimiento;
        }, getImages: state => {
            return state.establecimiento.images
        },getEstablecimientos: state => {
            return state.establecimientos;
        },
        getCategorys: state => {
            return state.categorias;
        },
        getCategory: state => {
            return state.categoria;
        }
    }
})
